import tensorflow as tf
from utils import batch_matvec_mul


def loss_fun(xhat, x):
    loss = 0.
    for xhatk in xhat:
        lk = tf.keras.losses.MSE(x, xhatk)
        loss += lk
    # print "Only using the last layer loss"
    loss = lk
    # print "regularizations added"
    # loss +=  0.01 * tf.reduce_mean(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    return loss

