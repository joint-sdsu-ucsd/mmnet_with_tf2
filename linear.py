import tensorflow as tf
import numpy as np
from utils import *


class Linear(tf.Module):
    def __init__(self, information):
        super(Linear, self).__init__()
        params = information['params']
        self.NT = 2 * params['K']
        self.NR = 2 * params['N']
        self.batch_size = information['batch_size']
        self.theta_1 = tf.Variable(1., trainable=True)

    def MMNet_iid(self, shatt, rt, features):
        batch_size = self.batch_size
        H = features['H']
        W = tf.linalg.matrix_transpose(H)
        helper = {'W': W}
        zt = shatt + tf.square(self.theta_1) * batch_matvec_mul(W, rt)
        return zt, helper


