import tensorflow as tf
# from layer import Layer
from layer import layer
from denoiser import Denoiser
from linear import Linear


class Detector(tf.Module):
    def __init__(self, params, lgst_constel, x, y, H, noise_sigma, indices, batch_size):
        super(Detector, self).__init__()
        self.information = {'x': x, 'indices': indices, 'params': params, 'constellation': lgst_constel,
                            'batch_size': batch_size}
        self.features = {'y': y, 'H': H, 'noise_sigma': noise_sigma}
        self.NT = 2 * params['K']
        self.NR = 2 * params['N']
        self.L = params['L']
        self.linear_name = params['linear_name']
        self.denoiser_name = params['denoiser_name']
        self.denoiser_objs = [Denoiser(self.information) for _ in range(self.L)]
        self.linear_objs = [Linear(self.information) for _ in range(self.L)]

    def __call__(self):
        # def create_graph(self):
        batch_size = self.information['batch_size']
        xhatk = tf.zeros(shape=[batch_size, self.NT], dtype=tf.float32)
        rk = tf.cast(self.features['y'], tf.float32)
        onsager = tf.zeros(shape=[batch_size, self.NR], dtype=tf.float32)
        xhat = []
        helper = {}
        # layerk = Layer(self.linear_name, self.denoiser_name, self.information)

        for k in range(1, self.L + 1):
            # [xhatk, rk, onsager, helperk, den_output] = (layer(xhatk, rk, onsager, self.features))
            xhatk, rk, onsager, helperk, den_output, denoise_obj, linear_obj = (
                layer(xhatk, rk, onsager, self.features,
                      self.denoiser_objs[k-1], self.denoiser_name,
                      self.linear_objs[k-1], self.linear_name,
                      self.information))
            xhat.append(den_output)
            helper['layer' + str(k)] = helperk
            # tf.Graph().add_to_collections(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES, layerk.trainable_variables)

        # trainable_variables = layerk.trainable_variables + layerk.get_trainable_variables()
        # print("Total number of trainable variables:", self.get_n_vars())
        return xhat, helper
