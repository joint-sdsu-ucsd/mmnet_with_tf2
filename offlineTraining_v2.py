import numpy as np
import tensorflow as tf
from tensorflow.python.ops import gen_math_ops
import os
import matplotlib as mpl
from detector import Detector
from sample_generator import generator

mpl.use('Agg')
from tf_session import *
import argparse

parser = argparse.ArgumentParser(description='MIMO signal detection simulator')

parser.add_argument('--x-size', '-xs',
                    type=int,
                    required=True,
                    help='Number of senders')

parser.add_argument('--y-size', '-ys',
                    type=int,
                    required=True,
                    help='Number of receivers')

parser.add_argument('--layers',
                    type=int,
                    required=True,
                    help='Number of neural net blocks')

parser.add_argument('--snr-min',
                    type=float,
                    required=True,
                    help='Minimum SNR in dB')

parser.add_argument('--snr-max',
                    type=float,
                    required=True,
                    help='Maximum SNR in dB')

parser.add_argument('--learn-rate', '-lr',
                    type=float,
                    required=True,
                    help='Learning rate')

parser.add_argument('--batch-size',
                    type=int,
                    required=True,
                    help='Batch size')

parser.add_argument('--test-every',
                    type=int,
                    required=True,
                    help='number of training iterations before each test')

parser.add_argument('--train-iterations',
                    type=int,
                    required=True,
                    help='Number of training iterations')

parser.add_argument('--modulation', '-mod',
                    type=str,
                    required=True,
                    help='Modulation type which can be BPSK, 4PAM, or MIXED')

parser.add_argument('--gpu',
                    type=str,
                    required=False,
                    default="0",
                    help='Specify the gpu core')

parser.add_argument('--test-batch-size',
                    type=int,
                    required=True,
                    help='Size of the test batch')

parser.add_argument('--data',
                    action='store_true',
                    help='Use dataset to train/test')

parser.add_argument('--linear',
                    type=str,
                    required=True,
                    help='linear transformation step method')

parser.add_argument('--denoiser',
                    type=str,
                    required=True,
                    help='denoiser function model')

parser.add_argument('--exp',
                    type=str,
                    required=False,
                    help='experiment name')

parser.add_argument('--corr-analysis',
                    action='store_true',
                    help='fetch covariance matrices')

parser.add_argument('--start-from',
                    type=str,
                    required=False,
                    default='',
                    help='Saved model name to start from')

parser.add_argument('--log',
                    action='store_true',
                    help='Log data mode')

args = parser.parse_args()
# os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu  # Ignore if you do not have multiple GPUs

# Simulation parameters
params = {
    'N': args.y_size,  # Number of receive antennas
    'K': args.x_size,  # Number of transmit antennas
    'L': args.layers,  # Number of layers
    'SNR_dB_min': tf.Variable(args.snr_min, dtype=tf.float32, trainable=False),
    'SNR_dB_max': tf.Variable(args.snr_max, dtype=tf.float32, trainable=False),
    'seed': 1,  # Seed for random number generation
    'batch_size': tf.Variable(args.batch_size, dtype=tf.int32, trainable=False),
    'modulation': args.modulation,
    'learn_rate': 1e-3,
    'correlation': False,
    'start_from': args.start_from,
    'data': args.data,
    'linear_name': args.linear,
    'denoiser_name': args.denoiser
}


def loss_computation(xhat, x):
    loss = 0.
    for xhatk in xhat:
        lk = tf.keras.losses.MSE(x, xhatk)
        loss += lk
        # tf.Graph().add_to_collections(tf.compat.v1.GraphKeys.LOSSES, lk)
    # print "Only using the last layer loss"
    loss = lk
    # print "regularizations added"
    # loss +=  0.01 * tf.reduce_mean(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    return tf.reduce_mean(loss)


batch_size = params['batch_size']
lr = params['learn_rate']
snr_db_max = params['SNR_dB_max']
snr_db_min = params['SNR_dB_min']
adam_optimizer = tf.keras.optimizers.Adam(lr)

# MIMO sample generator model
mimo = generator(params, batch_size)
constellation = mimo.constellation
indices = mimo.random_indices()
x = mimo.modulate(indices)
# Send x through the channel
y, H, noise_sigma, actual_snrdB = (mimo.channel(x, snr_db_min, snr_db_max, [], params['data'], params['correlation']))

model = Detector(params, constellation, x, y, H, noise_sigma, indices, batch_size)
# x_NN, helper = model()

test_data = []
train_data = []


def train_step(model):
    with tf.GradientTape() as tape:
        x_NN, _ = model()
        loss = loss_computation(x_NN, x)
    gradients = tape.gradient(loss, model.trainable_variables)
    adam_optimizer.apply_gradients(zip(gradients, model.trainable_variables))
    return loss


for it in range(args.train_iterations):
    # train_loss_avg = tf.keras.metrics.Mean()
    loss = train_step(model)
    # train_loss_avg.update_state(loss)
    if it % 10 == 0:
        print(f'Iter: {it + 1}, Loss: {loss}')

# TODO: Testing part comes soon
