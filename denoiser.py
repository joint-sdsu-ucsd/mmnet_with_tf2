import tensorflow as tf
from utils import *


class Denoiser(tf.Module):
    def __init__(self, information):
        super(Denoiser, self).__init__()
        params = information['params']
        lgst_constel = information['constellation']
        self.NT = 2 * params['K']
        self.NR = 2 * params['N']
        self.L = params['L']
        self.lgst_constel = tf.cast(lgst_constel, tf.float32)
        self.M = int(lgst_constel.shape[0])
        self.information = information
        self.theta_2 = tf.Variable(tf.random.normal([1, self.NT, 1], 1., 0.1), trainable=True)

    def gaussian(self, zt, features):
        tau2_t = features['tau2_t']
        arg = tf.reshape(zt, [-1, 1]) - self.lgst_constel
        arg = tf.reshape(arg, [-1, self.NT, self.M])
        arg = - tf.square(arg) / 2. / tau2_t
        arg = tf.reshape(arg, [-1, self.M])
        shatt1 = tf.nn.softmax(arg, axis=1)
        shatt1 = tf.matmul(shatt1, tf.reshape(self.lgst_constel, [self.M, 1]))
        shatt1 = tf.reshape(shatt1, [-1, self.NT])
        denoiser_helper = {}
        return shatt1, denoiser_helper

    def MMNet(self, zt, xhatt, rt, features, linear_helper):
        H = features['H']
        noise_sigma = features['noise_sigma']
        W_t = linear_helper['W']
        HTH = tf.matmul(H, H, transpose_a=True)
        v2_t = tf.divide(
            (tf.math.reduce_sum(tf.square(rt), axis=1, keepdims=True) - self.NR * tf.square(noise_sigma) / 2.),
            tf.expand_dims(tf.linalg.trace(HTH), axis=1))
        v2_t = tf.maximum(v2_t, 1e-9)
        v2_t = tf.expand_dims(v2_t, axis=2)
        C_t = tf.eye(self.NT, batch_shape=[tf.shape(H)[0]]) - tf.matmul(W_t, H)
        tau2_t = 1. / self.NT * tf.reshape(tf.linalg.trace(tf.matmul(C_t, C_t, transpose_b=True)),
                                           [-1, 1, 1]) * v2_t + tf.square(tf.reshape(noise_sigma, [-1, 1, 1])) / (
                         2. * self.NT) * tf.reshape(tf.linalg.trace(tf.matmul(W_t, W_t, transpose_b=True)), [-1, 1, 1])
        shatt1, _ = self.gaussian(
            zt, {'tau2_t': tau2_t / self.theta_2})
        denoiser_helper = {'onsager': tf.Variable(0., trainable=False)}
        return shatt1, denoiser_helper, shatt1

