import numpy as np
import tensorflow as tf
from tensorflow.python.ops import gen_math_ops
import os
import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as plt
from tf_session import *
import argparse
import pickle

parser = argparse.ArgumentParser(description='MIMO signal detection simulator')

parser.add_argument('--x-size', '-xs',
                    type=int,
                    required=True,
                    help='Number of senders')

parser.add_argument('--y-size', '-ys',
                    type=int,
                    required=True,
                    help='Number of receivers')

parser.add_argument('--layers',
                    type=int,
                    required=True,
                    help='Number of neural net blocks')

parser.add_argument('--snr-min',
                    type=float,
                    required=True,
                    help='Minimum SNR in dB')

parser.add_argument('--snr-max',
                    type=float,
                    required=True,
                    help='Maximum SNR in dB')

parser.add_argument('--learn-rate', '-lr',
                    type=float,
                    required=True,
                    help='Learning rate')

parser.add_argument('--batch-size',
                    type=int,
                    required=True,
                    help='Batch size')

parser.add_argument('--test-every',
                    type=int,
                    required=True,
                    help='number of training iterations before each test')

parser.add_argument('--train-iterations',
                    type=int,
                    required=True,
                    help='Number of training iterations')

parser.add_argument('--modulation', '-mod',
                    type=str,
                    required=True,
                    help='Modulation type which can be BPSK, 4PAM, or MIXED')

parser.add_argument('--gpu',
                    type=str,
                    required=False,
                    default="0",
                    help='Specify the gpu core')

parser.add_argument('--test-batch-size',
                    type=int,
                    required=True,
                    help='Size of the test batch')

parser.add_argument('--data',
                    action='store_true',
                    help='Use dataset to train/test')

parser.add_argument('--linear',
                    type=str,
                    required=True,
                    help='linear transformation step method')

parser.add_argument('--denoiser',
                    type=str,
                    required=True,
                    help='denoiser function model')

parser.add_argument('--exp',
                    type=str,
                    required=False,
                    help='experiment name')

parser.add_argument('--corr-analysis',
                    action='store_true',
                    help='fetch covariance matrices')

parser.add_argument('--start-from',
                    type=str,
                    required=False,
                    default='',
                    help='Saved model name to start from')

parser.add_argument('--log',
                    action='store_true',
                    help='Log data mode')

args = parser.parse_args()
# os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu  # Ignore if you do not have multiple GPUs

# Simulation parameters
params = {
    'N': args.y_size,  # Number of receive antennas
    'K': args.x_size,  # Number of transmit antennas
    'L': args.layers,  # Number of layers
    'SNR_dB_min': tf.Variable(args.snr_min, dtype=tf.float32, trainable=False),
    # Minimum SNR value in dB for training and evaluation
    'SNR_dB_max': tf.Variable(args.snr_max, dtype=tf.float32, trainable=False),
    # Maximum SNR value in dB for training and evaluation
    'seed': 1,  # Seed for random number generation
    'batch_size': tf.Variable(args.batch_size, dtype=tf.int32, trainable=False),
    'modulation': args.modulation,
    #    'TL': args.train_layer,
    'learn_rate': tf.Variable(args.learn_rate, dtype=tf.float32, trainable=False),
    'correlation': False,
    #    'save_name': args.saveas,
    'start_from': args.start_from,
    'data': args.data,
    'linear_name': args.linear,
    'denoiser_name': args.denoiser
}


def complex_to_real(inp):
    Hr = np.real(inp)
    Hi = np.imag(inp)
    h1 = np.concatenate([Hr, -Hi], axis=2)
    h2 = np.concatenate([Hi, Hr], axis=2)
    inp = np.concatenate([h1, h2], axis=1)
    return inp


# Build the computational graph
mmnet = MMNet_graph(params)
nodes = mmnet.build()

# Get access to the nodes on the graph
x = nodes['x']
x_NN = nodes['x_NN']
H = nodes['H']
x_id = nodes['x_id']
constellation = nodes['constellation']
train_optimizer = nodes['train_optimizer']

snr_db_min = nodes['snr_db_min']
snr_db_max = nodes['snr_db_max']
lr = nodes['lr']
batch_size = nodes['batch_size']
accuracy = nodes['accuracy']
mmse_accuracy = nodes['mmse_accuracy']
loss = nodes['loss']
logs = nodes['logs']
measured_snr = nodes['measured_snr']

record = {'before': [], 'after': []}
record_flag = False
trainable_variables = nodes['trainable_variables']

test_data = []
train_data = []


def train_step(last_loss, train_optimizer, model_variables):
    with tf.GradientTape() as tape:
        tape.watch(model_variables)
        loss = tf.reduce_mean(last_loss)
    gradients = tape.gradient(loss, model_variables)
    train_optimizer.apply_gradients(zip(gradients, model_variables))
    # train_optimizer.minimize(last_loss, model_variables)
    return loss


for it in range(args.train_iterations):
    train_loss_avg = tf.keras.metrics.Mean()
    loss = train_step(loss, train_optimizer, trainable_variables)
    train_loss_avg.update_state(loss)
    print(f'Iter: {iter + 1}, Loss: {train_loss_avg.result():.4f}')

# Train
# for it in range(args.train_iterations):
#     feed_dict = {
#         batch_size: args.batch_size,
#         lr: args.learn_rate,
#         snr_db_max: params['SNR_dB_max'],
#         snr_db_min: params['SNR_dB_min'],
#     }
#     if record_flag:
#         feed_dict_test = {
#             batch_size: args.test_batch_size,
#             lr: args.learn_rate,
#             snr_db_max: params['SNR_dB_max'],
#             snr_db_min: params['SNR_dB_min'],
#             # train_layer_no: tln_,
#             # H: train_data[np.tile(sample_ids[0],(args.test_batch_size))],
#         }
#
#         before_acc = 1. - sess.run(accuracy, feed_dict_test)
#         record['before'].append(before_acc)
#
#     # _, train_summary_ = sess.run([train, summary], feed_dict)
#     sess.run(train, feed_dict)


# Test
#     if (it % args.test_every == 0):
#         feed_dict = {
#             batch_size: args.test_batch_size,
#             snr_db_max: params['SNR_dB_max'],
#             snr_db_min: params['SNR_dB_max'],
#             # train_layer_no: tln_,
#         }
#         if args.log:
#             # test_accuracy_, test_loss_, logs_, x_, H_, test_summary_= sess.run([accuracy, loss, logs, x, H, summary], feed_dict)
#             test_accuracy_, test_loss_, logs_, x_, H_ = sess.run([accuracy, loss, logs, x, H], feed_dict)
#             np.save('log.npy', logs_)
#             break
#         else:
#             # if args.data:
#             # result = model_eval(test_data, params['SNR_dB_min'], params['SNR_dB_max'], mmse_accuracy, accuracy, batch_size, snr_db_min, snr_db_max, H, sess)
#             # print "iteration", it, result
#             # test_accuracy_, test_loss_, test_summary_, measured_snr_ = sess.run([accuracy, loss, summary, measured_snr], feed_dict)
#             test_accuracy_, test_loss_, measured_snr_ = sess.run([accuracy, loss, measured_snr], feed_dict)
#             print((it, 'SER: {:.2E}'.format(1. - test_accuracy_), test_loss_, measured_snr_))
#         if args.corr_analysis:
#             log_ = sess.run(logs, feed_dict)
#             for l in range(1, int(args.layers) + 1):
#                 c = log_['layer' + str(l)]['linear']['I_WH']
#                 print((np.linalg.norm(c, axis=(1, 2))[0]))
#
#             # temp2 = log_['layer'+str(l)]['linear']
#             # np.save('W'+str(l)+'.npy', temp2['W'])
#             # np.save('H'+str(l)+'.npy', temp2['H'])
#             # np.save('WsetR'+str(l)+'.npy', temp2['WsetR'])
#             # np.save('WsetI'+str(l)+'.npy', temp2['WsetI'])
#             # print "Written"
#
#             # np.save('Vw'+str(l)+'.npy', temp2['svd'][2][0])
#             # np.save('Uh'+str(l)+'.npy', temp2['svd'][4][0])
#             # np.save('Uw'+str(l)+'.npy', temp2['svd'][1][0])
#             # np.save('WH'+str(l)+'.npy', temp2['WH'][0])
#             # print np.matmul(temp2[1][0], temp2[4][0].conj().T)
#             # print temp2['VhVwt'][0]
#             # print temp2['norm_diff_U'][0], temp2['norm_Uw'][0], temp2['norm_diff_V'][0], temp2['norm_V'][0]
#         # saver.save(sess, './reports/'+args.saveas, global_step=it)
#         # test_summary_writer.add_summary(test_summary_, it)
#         # train_summary_writer.add_summary(train_summary_, it)
#
# result = model_eval(test_data, params['SNR_dB_min'],
#                     params['SNR_dB_max'], mmse_accuracy, accuracy, batch_size,
#                     snr_db_min, snr_db_max, H, sess)
# print(result)


# SNR_dBs = np.linspace(params['SNR_dB_min'],params['SNR_dB_max'],params['SNR_dB_max']-params['SNR_dB_min']+1)
# accs_mmse = np.zeros(shape=SNR_dBs.shape)
# accs_NN = np.zeros(shape=SNR_dBs.shape)
# iterations = 30
## SER Simulations
##print "PERTURBATIONS ON"
# if args.data:
#    test_data = test_data_ref
# for i in range(SNR_dBs.shape[0]):
#    noise_ = []
#    error_ = []
#    for j in range(iterations):
#        #pertr = np.random.normal(0.,0.01, (5000, 64, 16))
#        #perti = np.random.normal(0.,0.01, (5000, 64, 16))
#        #pert = np.concatenate([np.concatenate([pertr,-perti], axis=2), np.concatenate([perti,pertr], axis=2)], axis=1)
#        #test_data  = test_data_ref  + pert
#        feed_dict = {
#                batch_size: 5000,
#                snr_db_max: SNR_dBs[i],
#                snr_db_min: SNR_dBs[i],
#                train_layer_no: params['TL'],
#            }    
#        if args.data:
#            sample_ids = np.random.randint(0, np.shape(test_data)[0], 5000)
#            feed_dict[H] = test_data[sample_ids]
#        acc = sess.run([mmse_accuracy, accuracy], feed_dict)
#        accs_mmse[i] += acc[0]/iterations
#        accs_NN[i] += acc[1]/iterations
#    print "SER_mmse: ", 1. - accs_mmse 
#    print "SER_NN: ", 1. - accs_NN
